#include <cs50.h>
#include <stdio.h>
#include <math.h>

/**
 * prints the minimum number of coins that add up to
 * the input dollar amount
 * 
 * no params, no value returned - all io done within function
 */
int main(void)
{
    int coins[4] = {25, 10, 5, 1};
    int coins_length = 4;
    int coins_total = 0;
    int i, count;
    float change;
        
    // get a valid input    
    do 
    {
        printf("O hai! How much change is owed?\n");
        change = GetFloat();
    } while (change < 0);
    
    // convert the input to "number of pennies"
    change = round(change * 100);
    
    // greedily count up the coins needed to pay out the amount
    for (i = 0; i < coins_length; i++)
    {
        count = floor(change / coins[i]);
        coins_total += count;
        change -= count * coins[i];
        if (change == 0) break;
    }
    
    printf("%i\n", coins_total);
    return 0;
}