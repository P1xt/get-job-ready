#include <cs50.h>
#include <stdio.h>

/**
 * prints a triangular "staircase" of the height
 * specified by user input
 * 
 * no params, no value returned - all io done within function
 */
int main(void)
{
    int height = 0;
    int character = 0;
    int line = 0;
    
    // get a valid height
    do
    {
        printf("height: ");
        height = GetInt();
    } while (height < 0 || height >23);
    
    // print the corresponding pyramid
    for (line = 1; line <= height; line++) 
    {
        for (character = height - line; character > 0; character--)  { printf(" "); } // print the spaces
        for (character = 1; character <= line + 1; character++)  { printf("#"); }     // print the #s
 
        printf("\n");
    }
    
    return 0;
}