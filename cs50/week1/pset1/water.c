#include <cs50.h>
#include <stdio.h>

/**
 * converts the number of minutes one spends in the shower
 * to an equivalent number of bottles of water used
 * 
 * no params, no value returned - all io done within function
 */
int main(void)
{
    int minutes = 0;
    int BOTTLES_PER_MINUTE = 12;
    
    printf("minutes: ");
    minutes = GetInt();
    printf("bottles: %i\n", minutes * BOTTLES_PER_MINUTE);
    
    return 0;
}